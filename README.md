# Flask Demo App

## Description

 - Simple python web server that retrieve "Hello World!"

## Build

 - The only required step is to restore it depedencies by run:

```
pip install -r requirements.txt
```

## Usage

 - To run the application just run:
```
python app.py
```

 - Then you will be able to access the application on port 5000:
```
http://localhost:5000
```
