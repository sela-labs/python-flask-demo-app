# Basic Flask Server (run on port 5000 by default)

from flask import Flask
from flask import request
from flask import jsonify

app = Flask(__name__)

@app.route("/", methods=["GET"])
def index():
    return "<h1>Hello World!</h1>"

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')